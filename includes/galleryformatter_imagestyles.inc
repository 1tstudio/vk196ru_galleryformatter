<?php

/**
 * @file
 * Provides default Image styles presets that can be overridden by site administrators.
 */

/*
 * Implementation of hook_image_default_styles().
 */
function galleryformatter_image_default_styles() {
  $styles = array();

  $styles['galleryformatter_slide'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale_and_crop',
        'data' => array('width' => 270, 'height' => 270),
        'weight' => '0',
      ),
    ),
  );
  $styles['galleryformatter_thumb'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale_and_crop',
        'data' => array('width' => 60, 'height' => 60),
        'weight' => '0',
      ),
    ),
  );
  return $styles;
}

